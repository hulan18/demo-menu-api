package com.example.demo.menu.controller;

import com.example.demo.menu.entity.Menu;
import com.example.demo.menu.service.IMenuService;
import com.example.demo.security.error.EntityNotFoundException;
import com.example.demo.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/menu")
@Slf4j
@Validated
public class MenuController {

    @Autowired
    private IMenuService menuService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    List<Menu> all(OAuth2Authentication authentication) {
        String email = (String) authentication.getUserAuthentication().getPrincipal();
        String role = authentication.getAuthorities().iterator().next().getAuthority();

        return menuService.getMenuList(email, role);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    Boolean create(@Valid @RequestBody Menu res) {
        return menuService.addMenu(res);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    boolean delete(@PathVariable Long id) {
        Menu menu = menuService.getMenuById(id);

        if (menu != null) {
            if (menu.getUsers().size() == 0) {
                menuService.deleteMenu(id);
                return true;
            } else {
                return false;
            }
        } else {
            throw new EntityNotFoundException(Menu.class, "id", id.toString());
        }
    }
}
