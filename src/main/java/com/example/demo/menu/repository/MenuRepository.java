package com.example.demo.menu.repository;

import com.example.demo.menu.entity.Menu;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MenuRepository extends CrudRepository<Menu, Long> {
    List<Menu> findByName(String name);
    List<Menu> findAllByUsers_Email(String email);
}
