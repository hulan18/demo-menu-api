package com.example.demo.menu.entity;

import com.example.demo.user.entity.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name="menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    @Getter
    @Setter
    private long id;

    @Column(name="name")
    @Getter
    @Setter
    private String name;

    @ManyToMany(mappedBy = "menus", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @NotNull
    @ToString.Exclude
    @Getter
    @Setter
    private Collection<User> users;
}