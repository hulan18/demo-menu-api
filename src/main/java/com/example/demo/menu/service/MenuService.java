package com.example.demo.menu.service;

import com.example.demo.menu.entity.Menu;
import com.example.demo.menu.repository.MenuRepository;
import com.example.demo.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuService implements IMenuService {

    @Autowired
    private MenuRepository menuRepository;

    @Override
    public List<Menu> getMenuList(String email, String role) {
        if (role.equals(User.Role.ROLE_USER.name())) {
            return getAllMenuByUserEmail(email);
        }

        return getAllMenu();
    }

    @Override
    public List<Menu> getAllMenu() {
        List<Menu> list = new ArrayList<>();
        menuRepository.findAll().forEach(e -> list.add(e));
        return list;
    }

    @Override
    public List<Menu> getAllMenuByUserEmail(String email) {
        List<Menu> list = new ArrayList<>();
        menuRepository.findAllByUsers_Email(email).forEach(e -> list.add(e));
        return list;
    }

    @Override
    public Menu getMenuById(long id) {
        Menu obj = menuRepository.findById(id).get();
        return obj;
    }

    @Override
    public boolean addMenu(Menu menu) {
        List<Menu> list = menuRepository.findByName(menu.getName());

        if (list.size() > 0) {
            return false;
        } else {
            menuRepository.save(menu);
        }

        return true;
    }

    @Override
    public void updateMenu(Menu menu) {
        menuRepository.save(menu);
    }

    @Override
    public void deleteMenu(long id) {
        menuRepository.delete(getMenuById(id));
    }
}
