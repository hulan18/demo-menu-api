package com.example.demo.menu.service;

import com.example.demo.menu.entity.Menu;
import java.util.List;

public interface IMenuService {
    List<Menu> getMenuList(String email, String role);
    List<Menu> getAllMenu();
    List<Menu> getAllMenuByUserEmail(String email);
    Menu getMenuById(long id);
    boolean addMenu(Menu menu);
    void updateMenu(Menu menu);
    void deleteMenu(long id);
}
