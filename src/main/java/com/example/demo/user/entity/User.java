package com.example.demo.user.entity;

import com.example.demo.menu.entity.Menu;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="user", uniqueConstraints = {@UniqueConstraint(columnNames = "email")})
public class User {

    public enum Role {ROLE_ADMIN, ROLE_USER}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    @Email
    @Getter
    @Setter
    private String email;

    @JsonIgnore
    @ToString.Exclude
    @Getter
    @Setter
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Getter
    @Setter
    private Role role;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_menu",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "menu_id"))
    @ToString.Exclude
    @Getter
    @Setter
    private Collection<Menu> menus;
}